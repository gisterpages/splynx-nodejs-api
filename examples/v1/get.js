// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// API key info from page: https://SPLYNX_DOMAIN/admin/administration/api-keys
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';

// Create new api object
const api = new SplynxApi(SPLYNX_HOST, API_KEY, API_SECRET);
api.version = SplynxApi.API_VERSION_1_0;

// If need see more info
// api.debug = true;

// Get list of all customers
api.get('admin/customers/customer').then(res => {
    console.log(res);
}).catch(err => {
    console.log('Errors with status ' + err.statusCode);
    console.log('error ', err);
});
