// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// API key info from page: https://SPLYNX_DOMAIN/admin/administration/api-keys
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';

// Create new api object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// If need see more info
// api.debug = true;

// Execute custom function when token renew
// api.on('renew_token', function (res) {
//     console.log('Renew token handler', res);
// });

// Execute custom function when tokens expired
// api.on('unauthorized', function (returnData, res, err, body) {
//     console.log('Unauthorized handler', returnData);
// });

api.login(SplynxApi.LOGIN_TYPE_API_KEY, {
    key: API_KEY,
    secret: API_SECRET
}).then(() => {
    // Get list of all customers
    api.get('admin/customers/customer', 1).then(res => {
        console.log(res);
    }).catch(err => {
        console.log('Errors with status ' + err.statusCode);
        console.log('error ', err);
    });
}).catch(err => {
    console.log('Error', err);
});
