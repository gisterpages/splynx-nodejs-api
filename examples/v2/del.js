// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// API key info from page: https://SPLYNX_DOMAIN/admin/administration/api-keys
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';

// Create new SplynxApi object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// If need see more info
// api.debug = true;

api.login(SplynxApi.LOGIN_TYPE_API_KEY, {
    key: API_KEY,
    secret: API_SECRET
}).then(() => {
    var customer_id = ''; // Put here your customer ID for delete

    if (customer_id == '') {
        console.log('Please set customer ID!');
    } else {
        api.del('admin/customers/customer', customer_id).then(res => {
            console.log('Customer #' + customer_id + ' was deleted');
        }).catch(err => {
            console.log('Failed delete customer #' + customer_id);
        });
    }
}).catch(err => {
    console.log('Error', err);
});
