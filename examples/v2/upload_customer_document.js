// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// Administrator info from: https://SPLYNX_DOMAIN/admin/administration/administrators
// Also you can login by customer, API key or session ID (see https://splynxv2rc.docs.apiary.io/#reference/auth/generate-access-token)
const ADMIN_LOGIN = 'YOUR_ADMINISTRATOR_LOGIN';
const ADMIN_PASSWORD = 'YOUR_ADMINISTRATOR_PASSWORD';

// Create new SplynxApi object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// If need see more info
// api.debug = true;

api.login(SplynxApi.LOGIN_TYPE_ADMIN, {
    login: ADMIN_LOGIN,
    password: ADMIN_PASSWORD
}).then(() => {
    // For get more info see https://splynxv2rc.docs.apiary.io/#reference/customers/customer-documents-upload-file/upload-file-for-customer-document
    let params = {
        file: '/tmp/your_file.txt',
    };

    // Your customer document ID
    let documentId = 71;

    api.post('admin/customers/customer-documents/' + documentId + '--upload', params).then(res => {
        console.log('File success uploaded');
    }).catch(err => {
        console.log('Failed upload file:');
        console.log('Error: ', err);
    });
}).catch(err => {
    console.log('Error', err);
});
