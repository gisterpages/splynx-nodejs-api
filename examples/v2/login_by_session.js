// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// Splynx session id
const SESSION_ID = 'YOUR_SESSION_ID';

// Create new api object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// Enable debug mode
// api.debug = true;

api.login(SplynxApi.LOGIN_TYPE_SESSION, {
    session_id: SESSION_ID
}).then(() => {
    // Get customer
    api.get('admin/customers/customer', 1).then(res => {
        console.log('Customer:', res);
    }).catch(err => {
        console.log('Error ', err);
    });
}).catch(err => {
    console.log('Error', err);
});
