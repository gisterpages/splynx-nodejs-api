// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// API key info from page: https://SPLYNX_DOMAIN/admin/administration/api-keys
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';

// Create new api object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// If need see more info
// api.debug = true;

// Login by API key
api.login(SplynxApi.LOGIN_TYPE_API_KEY, {
    key: API_KEY,
    secret: API_SECRET
}).then(() => {
    // Get count of active customers
    let condition = {
        main_attributes: {
            status: 'active'
        }
    };
    api.count('admin/customers/customer', condition).then(count => {
        console.log('Count of active customers:', count);
    }).catch(err => {
        console.log('Error ', err);
    });
}).catch(err => {
    console.log('Error', err);
});


