// Include splynx-nodejs-api library
const SplynxApi = require('splynx-nodejs-api');

// With protocol and port if needed
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';

// API key info from page: https://SPLYNX_DOMAIN/admin/administration/api-keys
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';

// Create new SplynxApi object
const api = new SplynxApi(SPLYNX_HOST);
api.version = SplynxApi.API_VERSION_2_0;

// If need see more info
// api.debug = true;

api.login(SplynxApi.LOGIN_TYPE_API_KEY, {
    key: API_KEY,
    secret: API_SECRET
}).then(() => {
    // Customers data
    // For get more info see http://docs.splynx.apiary.io/#reference/customers/customers-collection/create-a-customer
    var postParams = {
        'partner_id': '1',
        'location_id': '1',
        'billing_type': 'recurring',
        'added_by_id': '1',
        'login': 'test_customer_' + Math.floor(Math.random() * 9999),
        'password': 'good_password123',
        'name': 'John Helper',
        'email': 'john@help.com',
        'phone': '84348478378'
    };

    // Api methods return promise
    api.post('admin/customers/customer', postParams).then(res => {
        console.log('Success create customer with id #' + res.response.id);
    }).catch(err => {
        console.log('Failed create new customer:');
        console.log('Error: ', err);
    });
}).catch(err => {
    console.log('Error', err);
});
