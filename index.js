/**
 * Splynx Node.js API v. 1.0.8
 * Splynx API Class
 * Author: Tsumanchuk Volodymyr
 * http://docs.splynx.apiary.io/ - documentation
 */

'use strict';

const SplynxApi = require('./api');
const http_build_query = require('qhttp/http_build_query');

class ApiHelper {
    constructor(host, api_key, api_secret) {
        this.api = new SplynxApi(host, api_key, api_secret);
        this.api.version = SplynxApi.API_VERSION_1_0;
    }

    static get API_VERSION_1_0() {
        return SplynxApi.API_VERSION_1_0;
    }

    static get API_VERSION_2_0() {
        return SplynxApi.API_VERSION_2_0;
    }

    static get LOGIN_TYPE_ADMIN() {
        return SplynxApi.LOGIN_TYPE_ADMIN;
    }

    static get LOGIN_TYPE_CUSTOMER() {
        return SplynxApi.LOGIN_TYPE_CUSTOMER;
    }

    static get LOGIN_TYPE_API_KEY() {
        return SplynxApi.LOGIN_TYPE_API_KEY;
    }

    static get LOGIN_TYPE_SESSION() {
        return SplynxApi.LOGIN_TYPE_SESSION;
    }

    static get CONTENT_TYPE_APPLICATION_JSON() {
        return SplynxApi.CONTENT_TYPE_APPLICATION_JSON;
    }

    static get CONTENT_TYPE_MULTIPART_FORM_DATA() {
        return SplynxApi.CONTENT_TYPE_MULTIPART_FORM_DATA;
    }

    set sash(value) {
        this.api.sash = value;
    }

    get administrator() {
        return this.api.administrator;
    }

    set debug(value) {
        this.api.debug = value;
    }

    set version(value) {
        this.api.version = value;
    }

    get version() {
        return this.api.version;
    }

    /**
     * Set event handler.
     * Allowed events: `unauthorized`, `renew_token`.
     * @param {string} event
     * @param {callback} callback
     */
    on(event, callback) {
        if (typeof this.api['on_' + event + '_callback'] !== 'undefined') {
            this.api['on_' + event + '_callback'] = callback;
        }
    }

    get(url, id) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'GET',
                path: url
            };

            if (typeof id !== 'undefined') {
                options.path += '/' + id;
            }

            this.api.process(options, (res) => {
                if (res.statusCode == 200) {
                    resolve(res);
                } else {
                    reject(res);
                }
            })
        });
    }

    post(url, params, contentType) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'POST',
                contentType: contentType,
                path: url,
                params: params
            };

            this.api.process(options, (res) => {
                if (res.statusCode == 201) {
                    resolve(res);
                } else {
                    reject(res);
                }
            });
        });
    }

    put(url, id, params, contentType) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'PUT',
                contentType: contentType,
                path: url + '/' + id,
                params: params
            };

            this.api.process(options, (res) => {
                if (res.statusCode == 202) {
                    resolve(res);
                } else {
                    reject(res);
                }
            });
        });
    }

    del(url, id) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'DELETE',
                path: url + '/' + id
            };

            this.api.process(options, (res) => {
                if (res.statusCode == 204) {
                    resolve(res);
                } else {
                    reject(res);
                }
            });
        });
    }

    search(url, params) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'GET',
                path: url + '?' + http_build_query(params)
            };

            this.api.process(options, (res) => {
                if (res.statusCode == 200) {
                    resolve(res);
                } else {
                    reject(res);
                }
            })
        });
    }

    options(url) {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'OPTIONS',
                path: url
            };

            this.api.process(options, (res) => {
                if (this.debug) {
                    console.log('options response', res);
                }

                if (res.statusCode == 200) {
                    resolve(res);
                } else {
                    reject(res);
                }
            });
        });
    }

    count(url, params) {
        return new Promise((resolve, reject) => {
            let path = url;
            if (typeof params !== 'undefined') {
                path += '?' + http_build_query(params);
            }

            let options = {
                method: 'HEAD',
                path: path
            };

            this.api.process(options, (result, response) => {
                if (result.statusCode == 204 && SplynxApi.HEADER_X_TOTAL_COUNT in response.headers) {
                    resolve(response.headers[SplynxApi.HEADER_X_TOTAL_COUNT]);
                } else {
                    reject(result);
                }
            });
        });
    }

    login(auth_type, data) {
        return new Promise((resolve, reject) => {
            // Prevent using login in API v1
            if (this.api.version === SplynxApi.API_VERSION_1_0) {
                reject('Method `login` not allowed in API 1.0! See examples for more information.');
                return;
            }

            data['auth_type'] = auth_type;
            this.api.login(data)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    logout() {
        return new Promise((resolve, reject) => {
            this.api.logout()
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    getAuthData() {
        return this.api.getAuthData();
    }

    setAuthData(auth_data) {
        this.api.setAuthData(auth_data);
    }
}

module.exports = ApiHelper;