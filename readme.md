Splynx Node.js api module
========================

Use this module for work with Splynx API into Node.js

How to install
--------------
Go to your project directory

Install **splynx-nodejs-api** library
```
npm install splynx-nodejs-api
```

Run example for API v1.0
------------------------

Copy example file for **API v1.0**
```
cp node_modules/splynx-nodejs-api/examples/v1/get.js example.js
```

Or copy example file for **API v2.0**
```
cp node_modules/splynx-nodejs-api/examples/v2/get.js example.js
```

Edit example file
```
nano example.js
```

Replace the constants with your data
```
const SPLYNX_HOST = 'YOUR_SPLYNX_HOST';
const API_KEY = 'YOUR_SPLYNX_API_KEY';
const API_SECRET = 'YOUR_SPLYNX_API_SECRET';
```

Run example
```
node example.js
```

API version
-----------
Required Splynx version according to API versions.

| API version | Splynx version |
| ----------- | -------------- |
| 1.0 | <= 2.2 |
| 2.0 | >= 2.3 |

Examples
--------

See examples into **examples** directory

Documentation
-------------

[Splynx API documentation](http://docs.splynx.apiary.io/#)